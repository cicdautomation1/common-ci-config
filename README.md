# common-ci-config


This is a test repo for _distibuting **gitlab-ci.yml** across repos with **Push** model_


## Getting started

1. A **common-ci-config** project is created, in which the common **.gitlab-ci.yml** is stored and other files needed for the build;
2. A **gitlab-ci-distributor** need to be created, who is given the rights to **push** (Main/Master) into the necessary projects.
